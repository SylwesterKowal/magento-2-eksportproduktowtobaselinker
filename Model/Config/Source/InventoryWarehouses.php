<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model\Config\Source;

class InventoryWarehouses implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Kowal\EksportProduktowToBaselinker\Helper\Send $sendHelper

    )
    {
        //  parent::__construct();
        $this->sendHelper = $sendHelper;

    }

    public function toOptionArray()
    {
        $methodParams = '{}';
        $apiParams = [
            "method" => "getInventoryWarehouses",
            "parameters" => $methodParams
        ];
        $inventories = [];
        if ($bl_catalogs = $this->sendHelper->send($apiParams)) {
            if (isset($bl_catalogs['warehouses'])) {
                foreach ($bl_catalogs['warehouses'] as $inventory) {
                    $inventories[] = ['value' => $inventory['warehouse_type'].'_'.$inventory['warehouse_id'], 'label' => $inventory['name']];
                }
            }
        }
        return $inventories;
    }

    public
    function toArray()
    {
        $inventories = $this->toOptionArray();
        $arr = [];
        foreach ($inventories as $inventory) {
            $arr[$inventory['value']] = $inventory['label'];
        }
        return $arr;
    }
}

