<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model\Config\Source;

class Sources implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository

    )
    {
        //  parent::__construct();
        $this->sourceRepository = $sourceRepository;

    }

    public function toOptionArray()
    {
        $sourceData = $this->sourceRepository->getList();
        $soources_ = [];
        if ($soources = $sourceData->getItems()) {
                foreach ($soources as $soource) {
                    $soources_[] = ['value' => $soource['source_code'], 'label' => $soource['name']];
                }

        }
        return $soources_;
    }

    public
    function toArray()
    {
        $inventories = $this->toOptionArray();
        $arr = [];
        foreach ($inventories as $inventory) {
            $arr[$inventory['value']] = $inventory['label'];
        }
        return $arr;
    }
}

