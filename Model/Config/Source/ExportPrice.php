<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model\Config\Source;

class ExportPrice implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'netto', 'label' => __('Netto')],['value' => 'brutto', 'label' => __('Brutto')]];
    }

    public function toArray()
    {
        return ['netto' => __('Netto'),'brutto' => __('Brutto')];
    }
}
