<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model;

use Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface;
use Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class BaselinkerIds extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'kowal_eksportproduktowtobaselinker_baselinkerids';
    protected $baselinkeridsDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BaselinkerIdsInterfaceFactory $baselinkeridsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds $resource
     * @param \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BaselinkerIdsInterfaceFactory $baselinkeridsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds $resource,
        \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds\Collection $resourceCollection,
        array $data = []
    ) {
        $this->baselinkeridsDataFactory = $baselinkeridsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve baselinkerids model with baselinkerids data
     * @return BaselinkerIdsInterface
     */
    public function getDataModel()
    {
        $baselinkeridsData = $this->getData();
        
        $baselinkeridsDataObject = $this->baselinkeridsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $baselinkeridsDataObject,
            $baselinkeridsData,
            BaselinkerIdsInterface::class
        );
        
        return $baselinkeridsDataObject;
    }
}

