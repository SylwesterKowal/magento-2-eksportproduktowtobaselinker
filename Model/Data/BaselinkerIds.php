<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model\Data;

use Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface;

class BaselinkerIds extends \Magento\Framework\Api\AbstractExtensibleObject implements BaselinkerIdsInterface
{

    /**
     * Get baselinkerids_id
     * @return string|null
     */
    public function getBaselinkeridsId()
    {
        return $this->_get(self::BASELINKERIDS_ID);
    }

    /**
     * Set baselinkerids_id
     * @param string $baselinkeridsId
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     */
    public function setBaselinkeridsId($baselinkeridsId)
    {
        return $this->setData(self::BASELINKERIDS_ID, $baselinkeridsId);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get bl_id
     * @return string|null
     */
    public function getBlId()
    {
        return $this->_get(self::BL_ID);
    }

    /**
     * Set bl_id
     * @param string $blId
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     */
    public function setBlId($blId)
    {
        return $this->setData(self::BL_ID, $blId);
    }
}

