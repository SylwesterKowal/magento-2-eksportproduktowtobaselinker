<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'baselinkerids_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\EksportProduktowToBaselinker\Model\BaselinkerIds::class,
            \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds::class
        );
    }
}

