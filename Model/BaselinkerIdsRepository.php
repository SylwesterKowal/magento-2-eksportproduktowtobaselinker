<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Model;

use Kowal\EksportProduktowToBaselinker\Api\BaselinkerIdsRepositoryInterface;
use Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterfaceFactory;
use Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsSearchResultsInterfaceFactory;
use Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds as ResourceBaselinkerIds;
use Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds\CollectionFactory as BaselinkerIdsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class BaselinkerIdsRepository implements BaselinkerIdsRepositoryInterface
{

    protected $baselinkerIdsFactory;

    protected $dataBaselinkerIdsFactory;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    private $collectionProcessor;

    private $storeManager;

    protected $searchResultsFactory;

    protected $baselinkerIdsCollectionFactory;

    protected $resource;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourceBaselinkerIds $resource
     * @param BaselinkerIdsFactory $baselinkerIdsFactory
     * @param BaselinkerIdsInterfaceFactory $dataBaselinkerIdsFactory
     * @param BaselinkerIdsCollectionFactory $baselinkerIdsCollectionFactory
     * @param BaselinkerIdsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceBaselinkerIds $resource,
        BaselinkerIdsFactory $baselinkerIdsFactory,
        BaselinkerIdsInterfaceFactory $dataBaselinkerIdsFactory,
        BaselinkerIdsCollectionFactory $baselinkerIdsCollectionFactory,
        BaselinkerIdsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->baselinkerIdsFactory = $baselinkerIdsFactory;
        $this->baselinkerIdsCollectionFactory = $baselinkerIdsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataBaselinkerIdsFactory = $dataBaselinkerIdsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface $baselinkerIds
    ) {
        /* if (empty($baselinkerIds->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $baselinkerIds->setStoreId($storeId);
        } */
        
        $baselinkerIdsData = $this->extensibleDataObjectConverter->toNestedArray(
            $baselinkerIds,
            [],
            \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface::class
        );
        
        $baselinkerIdsModel = $this->baselinkerIdsFactory->create()->setData($baselinkerIdsData);
        
        try {
            $this->resource->save($baselinkerIdsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the baselinkerIds: %1',
                $exception->getMessage()
            ));
        }
        return $baselinkerIdsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($baselinkerIdsId)
    {
        $baselinkerIds = $this->baselinkerIdsFactory->create();
        $this->resource->load($baselinkerIds, $baselinkerIdsId);
        if (!$baselinkerIds->getId()) {
            throw new NoSuchEntityException(__('BaselinkerIds with id "%1" does not exist.', $baselinkerIdsId));
        }
        return $baselinkerIds->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->baselinkerIdsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface $baselinkerIds
    ) {
        try {
            $baselinkerIdsModel = $this->baselinkerIdsFactory->create();
            $this->resource->load($baselinkerIdsModel, $baselinkerIds->getBaselinkeridsId());
            $this->resource->delete($baselinkerIdsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the BaselinkerIds: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($baselinkerIdsId)
    {
        return $this->delete($this->get($baselinkerIdsId));
    }
}

