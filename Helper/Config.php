<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{

    const SECTIONS      = 'exportproductstobl';   // module name
    const GROUPS        = 'settings';        // setup general

    /**
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var Random
     */
    protected $mathRandom;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\Math\Random $random
    ) {
        $this->_authorization = $authorization;
        $this->mathRandom     = $random;
        $this->storeManager     = $storeManager;

        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }




    public function getConfig($cfg=null,$store_id = 0)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );
    }

    public function getGeneralCfg($cfg=null,$store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS.'/'.self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if(isset($config[$cfg])) return $config[$cfg];
        return $config;
    }


    public function getBaselinkerToken()
    {
        if($token = $this->getGeneralCfg('baselinker_token', 0)){
            if(is_string($token)) return $token;
        }
        return "";
    }

}

