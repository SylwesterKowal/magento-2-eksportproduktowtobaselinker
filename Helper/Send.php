<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Send extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Config $configHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context             $context,
        \Kowal\EksportProduktowToBaselinker\Helper\Config $configHelper
    )
    {
        parent::__construct($context);
        $this->configHelper = $configHelper;
    }


    public function send($apiParams)
    {
        $curl = curl_init("https://api.baselinker.com/connector.php");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["X-BLToken: " . $this->configHelper->getBaselinkerToken()]);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($apiParams));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $errno = intval(curl_errno($curl));

        curl_close($curl);

        // sprawdzamy odpowiedz
        if (!$err) {
            return json_decode($response, true);
        } else {
            // problem z wykonaniem requestu
            var_dump($err, $errno);
            return false;
        }
    }
}

