<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Kowal\EksportProduktowToBaselinker\Helper\Export as ExportToBl;

class Update extends ExportToBl
{


    public function execute($type = 'stock')
    {

        if ($products = $this->getProductCollection()) {
            if ($getBaselinkerToken = $this->configHelper->getBaselinkerToken()) {
                $i = 1;
                $products_stock = [];
                $products_prices = [];
                foreach ($products as $product) {
                    //echo $i . " " . $product->getSku() . PHP_EOL;

                    if ($bl_product_id = $this->getBLproductId($product->getSku())) {
                        $products_stock[] = [$bl_product_id, 0, (int)round((float)$this->getStock($product),0)];
                        $products_prices[] = ['product_id' => $bl_product_id, 'variant_id'=> 0, 'price_brutto' => $this->getPrice($product),'tax_rate'=>str_replace("%", "", (string)$product->getResource()->getAttribute('tax_class_id')->getFrontend()->getValue($product))];
                    }
                    $i++;
                }

                switch ($type) {
                    case 'stock':
                        $products__ = array_chunk($products_stock, 1000);
                        foreach ($products__ as $cproduct) {
                            $apiParams = [
                                "method" => "updateProductsQuantity",
                                "parameters" => $this->setProduct($cproduct)
                            ];
                            // echo print_r($apiParams, true);
                            $bl_result = $this->sendHelper->send($apiParams);
                            if (isset($bl_product['status']) && $bl_product['status'] == 'SUCCESS') {

                            } else {
                                echo print_r($bl_result,true);
                            }
                            sleep(60);
                        }

                        break;
                    case 'price':
                        $products__ = array_chunk($products_prices, 1000);
                        foreach ($products__ as $cproduct) {
                            $apiParams = [
                                "method" => "updateProductsPrices",
                                "parameters" => $this->setProduct($cproduct)
                            ];
                            //echo print_r($apiParams, true);
                            $bl_result = $this->sendHelper->send($apiParams);
                            if (isset($bl_product['status']) && $bl_product['status'] == 'SUCCESS') {

                            } else {
                                echo print_r($bl_result,true);
                            }
                            sleep(60);
                        }
                        break;
                }
            }
        }
    }

    protected function setProduct($products)
    {
        return json_encode([
            'storage_id' =>  $this->configHelper->getGeneralCfg('bl_storages'),
            'products' => $products
        ]);

    }
}

