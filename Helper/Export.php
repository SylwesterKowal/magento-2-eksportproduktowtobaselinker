<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\CouldNotSaveException;

class Export extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context                                                   $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory                          $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface                                              $storeManager,
        \Magento\Framework\Api\SearchCriteriaBuilder                                            $searchCriteriaBuilder,
        \Magento\InventoryApi\Api\SourceItemRepositoryInterface                                 $sourceItemRepositoryInterface,
        \Magento\Catalog\Helper\Data                                                            $taxHelper,
        \Kowal\EksportProduktowToBaselinker\Helper\Send                                         $sendHelper,
        \Kowal\EksportProduktowToBaselinker\Helper\Config                                       $configHelper,
        \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds\CollectionFactory $baseLInkerIdsCollectionFactory,
        \Kowal\EksportProduktowToBaselinker\Model\ResourceModel\BaselinkerIds                   $baselinkerIdsResource,
        \Kowal\EksportProduktowToBaselinker\Model\BaselinkerIdsFactory                          $baselinkerIdsFactory
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sourceItemRepositoryInterface = $sourceItemRepositoryInterface;
        $this->taxHelper = $taxHelper;
        $this->sendHelper = $sendHelper;
        $this->configHelper = $configHelper;
        $this->baseLInkerIdsCollectionFactory = $baseLInkerIdsCollectionFactory;
        $this->baselinkerIdsResource = $baselinkerIdsResource;
        $this->baselinkerIdsFactory = $baselinkerIdsFactory;
        parent::__construct($context);

        $this->manufacturers = [];
    }

    public
    function execute()
    {
        if ($products = $this->getProductCollection()) {
            if ($getBaselinkerToken = $this->configHelper->getBaselinkerToken()) {

                $this->manufacturers = $this->getInventoryManufacturers();

                $i = 1;
                foreach ($products as $product) {

                    if($i >= 999) break;

                    $apiParams = [
                        "method" => "addInventoryProduct",
                        "parameters" => $this->setProduct($product)
                    ];

                    if (!$this->getBLproductId($product->getSku())) {

                        echo $i . " " . $product->getSku() . PHP_EOL;
                        //echo print_r($apiParams, true);

                        $bl_product = $this->sendHelper->send($apiParams);

                        if (isset($bl_product['status']) && $bl_product['status'] == 'SUCCESS') {
                            if (!$this->getBLproductSku($bl_product['product_id'])) {
                                $baselinkerIdsModel = $this->baselinkerIdsFactory->create()->setData(['sku' => $product->getSku(), 'bl_id' => $bl_product['product_id']]);
                                try {
                                    $this->baselinkerIdsResource->save($baselinkerIdsModel);
                                } catch (\Exception $exception) {
                                    throw new CouldNotSaveException(__(
                                        'Could not save the baselinkerIds: %1',
                                        $exception->getMessage() . PHP_EOL . print_r($apiParams, true)
                                    ));
                                }
                            }
                        }
                        $i++;
                    } else if ($this->configHelper->getGeneralCfg('update')) {
                        $bl_product = $this->sendHelper->send($apiParams);
                        if (isset($bl_product['status']) && $bl_product['status'] == 'SUCCESS') {
                            echo print_r($bl_product, true);
                        } else {
                            throw new CouldNotSaveException(__(
                                'Could not update: %1',
                                print_r($bl_product, true) . PHP_EOL . print_r($apiParams, true)
                            ));
                        }
                    }
                }
            }
        }
    }

    protected
    function getBLproductSku($bl_id)
    {
        $collection = $this->baseLInkerIdsCollectionFactory->create();
        $ids = $collection->addFieldToFilter('bl_id', ['eq' => $bl_id]);
        foreach ($ids as $id) {
            return $id->getSku();
        }
        return false;
    }

    protected
    function getBLproductId($sku)
    {
        $collection = $this->baseLInkerIdsCollectionFactory->create();
        $ids = $collection->addFieldToFilter('sku', ['eq' => $sku]);
        foreach ($ids as $id) {
            return $id->getBlId();
        }

        return "";
    }

    protected
    function getStock($product)
    {

        $qty = 0;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(\Magento\InventoryApi\Api\Data\SourceItemInterface::SKU, $product->getSku())
            ->create();

        $result = $this->sourceItemRepositoryInterface->getList($searchCriteria)->getItems();

        foreach ($result as $item) {

            // print_r($item->getData());
            /*
                [source_item_id] => 7
                [source_code] => default
                [sku] => 24-UB02
                [quantity] => 98.0000
                [status] => 1
             */

            if (isset($item['source_code']) && $item['source_code'] == $this->configHelper->getGeneralCfg('source_code')) {
                $qty = $item['quantity'];
            }
        }

        return $qty;
    }


    protected
    function getImage($product)
    {
        $images_ = $product->getMediaGalleryImages();
        $i = 0;
        $images = [];
        foreach ($images_ as $child) {
            if ($i <= 15) {
                $images[] = '"' . $i . '": ' . json_encode("url:" . $child->getUrl());
                $i++;
            }
        }
        return implode(",", $images);
    }

    public
    function getTaxRate($product)
    {
        return str_replace("%", "", $product->getResource()->getAttribute('tax_class_id')->getFrontend()->getValue($product));
    }

    protected
    function setProduct($product)
    {
        $gtin = $this->configHelper->getGeneralCfg('gtin') ?? 'ean';
        $description = $this->configHelper->getGeneralCfg('description') ?? 'description';


        return '{
                "inventory_id": "' . $this->configHelper->getGeneralCfg('bl_katalog_id') . '",
                "product_id": "' . $this->getBLproductId($product->getSku()) . '",
                "is_bundle": false,
                "ean": "' . $product->getData($gtin) . '",
                "sku": "' . $product->getSku() . '",
                "tax_rate": "' . $this->getTaxRate($product) . '",
                "weight": "' . $product->getWeight() . '",
                "manufacturer_id": "' . $this->setManufacturer($product) . '",
                "prices": {
                    "' . $this->configHelper->getGeneralCfg('bl_price_group_id') . '": ' . $this->getPrice($product) . '
                },
                "stock": {
                    "' . $this->configHelper->getGeneralCfg('bl_inventory_warehouses') . '": ' . $this->getStock($product) . '
                },
                "text_fields":' . $this->getTextFields($product) . ',
                "images": { ' . $this->getImage($product) . '},
                "links": {
                    "shop_' . $this->configHelper->getGeneralCfg('store_id') . '": {
                        "product_id": ' . $product->getId() . '
                    }
                }
            }';
    }

    protected function getPrice($product){
        if( $this->configHelper->getGeneralCfg('export_price') == 'brutto'){
            return $this->taxHelper->getTaxPrice($product, $product->getFinalPrice(), true);
        }else{
            return $product->getFinalPrice();
        }
    }

    private
    function getInventoryManufacturers()
    {
        $methodParams = '{}';
        $apiParams = [
            "method" => "getInventoryManufacturers",
            "parameters" => $methodParams
        ];
        $mList = [];
        $manufacturers = $this->sendHelper->send($apiParams);
        if (isset($manufacturers['status']) && $manufacturers['status'] == 'SUCCESS') {

            foreach ($manufacturers['manufacturers'] as $manufacturer) {
                $mList[strtoupper($manufacturer['name'])] = $manufacturer['manufacturer_id'];
            }
        }
        return $mList;
    }

    public
    function setManufacturer($product)
    {
        $manufacturer_code = $this->configHelper->getGeneralCfg('manufacturer') ?? 'manufacturer';
        $manufacturer = $product->getResource()->getAttribute($manufacturer_code)->getFrontend()->getValue($product);
        if (empty($manufacturer) || is_null($manufacturer)) return null;
        if (array_key_exists(strtoupper($manufacturer), $this->manufacturers)) {
            return $this->manufacturers[strtoupper($manufacturer)];
        } else {
            $methodParams = '{
                "name": "' . strtoupper($manufacturer) . '"
            }';
            $apiParams = [
                "method" => "addInventoryManufacturer",
                "parameters" => $methodParams
            ];
            $manufacturer_ = $this->sendHelper->send($apiParams);
            if (isset($manufacturer_['status']) && $manufacturer_['status'] == 'SUCCESS') {
                $this->manufacturers = $this->getInventoryManufacturers();
                return $manufacturer_['manufacturer_id'];
            }
        }
        return null;
    }

    private
    function getTextFields($product)
    {

        $description = $this->configHelper->getGeneralCfg('description') ?? 'description';
        $text = [
            "name" => $product->getName(),
            "description" => $product->getData($description),
            'description_extra1' => $product->getShortDescription()
        ];

        return json_encode($text);
    }

    public
    function getProductCollection()
    {
        if ($store_id = $this->configHelper->getGeneralCfg('store_id')) {
            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect('*');
            $collection->addMediaGalleryData();
            $collection->addStoreFilter($store_id);
            return $collection;
        } else {
            return false;
        }
    }
}

