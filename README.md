# Mage2 Module Kowal EksportProduktowToBaselinker

    ``kowal/module-eksportproduktowtobaselinker``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Eksport produktów do BL

## Installation

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_EksportProduktowToBaselinker`
 - Apply database updates by running `php bin/magento setup:upgrade --keep-generated`
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-eksportproduktowtobaselinker`
 - enable the module by running `php bin/magento module:enable Kowal_EksportProduktowToBaselinker`
 - apply database updates by running `php bin/magento setup:upgrade --keep-generated`
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (exportproductstobl/settings/enable)

 - baselinker_token (exportproductstobl/settings/baselinker_token)

 - store_id (exportproductstobl/settings/store_id)

 - bl_katalog_id (exportproductstobl/settings/bl_katalog_id)


## Specifications

 - Console Command
	- ExportProducts

 - Console Command
	- UpdateStock

 - Helper
	- Kowal\EksportProduktowToBaselinker\Helper\Config

 - Helper
	- Kowal\EksportProduktowToBaselinker\Helper\Export

 - Helper
	- Kowal\EksportProduktowToBaselinker\Helper\Update

 - Helper
	- Kowal\EksportProduktowToBaselinker\Helper\Send


## Attributes



