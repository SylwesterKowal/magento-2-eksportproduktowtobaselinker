<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdatePrice extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\EksportProduktowToBaselinker\Helper\Update $updateHelper
    )
    {
        parent::__construct();
        $this->updateHelper = $updateHelper;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Start Price" . $name);
        $this->updateHelper->execute('price');
    }

    /**
     * {@inheritdoc}
     * * * * * *  /usr/local/bin/php /home/xxxxxxx/public_html/bin/magento kowal_eksportproduktowtobaselinker:updatestock >> /home/xxxxxxx/public_html/var/log/baselinker-stocks.log
    */
    protected function configure()
    {
        $this->setName("kowal_eksportproduktowtobaselinker:updateprice");
        $this->setDescription("Aktualizacja Cen w BL");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

