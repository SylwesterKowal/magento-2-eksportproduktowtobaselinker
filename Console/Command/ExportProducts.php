<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportProducts extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\EksportProduktowToBaselinker\Helper\Export $exportHelper
    )
    {
        parent::__construct();
        $this->exportHelper = $exportHelper;

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Start " . $name);
        $this->exportHelper->execute();
    }

    /**
     * {@inheritdoc}
     *   0 0 * * *  /usr/local/bin/php /home/xxxxxxx/public_html/bin/magento kowal_eksportproduktowtobaselinker:exportproducts >> /home/xxxxxxxx/public_html/var/log/baselinker-products.log
     */
    protected function configure()
    {
        $this->setName("kowal_eksportproduktowtobaselinker:exportproducts");
        $this->setDescription("Eksport produktów");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

