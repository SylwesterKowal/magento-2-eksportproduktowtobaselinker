<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Api\Data;

interface BaselinkerIdsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const BL_ID = 'bl_id';
    const BASELINKERIDS_ID = 'baselinkerids_id';
    const SKU = 'sku';

    /**
     * Get baselinkerids_id
     * @return string|null
     */
    public function getBaselinkeridsId();

    /**
     * Set baselinkerids_id
     * @param string $baselinkeridsId
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     */
    public function setBaselinkeridsId($baselinkeridsId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     */
    public function setSku($sku);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsExtensionInterface $extensionAttributes
    );

    /**
     * Get bl_id
     * @return string|null
     */
    public function getBlId();

    /**
     * Set bl_id
     * @param string $blId
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     */
    public function setBlId($blId);
}

