<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Api\Data;

interface BaselinkerIdsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get BaselinkerIds list.
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

