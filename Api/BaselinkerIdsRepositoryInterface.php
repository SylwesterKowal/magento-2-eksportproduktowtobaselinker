<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\EksportProduktowToBaselinker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BaselinkerIdsRepositoryInterface
{

    /**
     * Save BaselinkerIds
     * @param \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface $baselinkerIds
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface $baselinkerIds
    );

    /**
     * Retrieve BaselinkerIds
     * @param string $baselinkeridsId
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($baselinkeridsId);

    /**
     * Retrieve BaselinkerIds matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete BaselinkerIds
     * @param \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface $baselinkerIds
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\EksportProduktowToBaselinker\Api\Data\BaselinkerIdsInterface $baselinkerIds
    );

    /**
     * Delete BaselinkerIds by ID
     * @param string $baselinkeridsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($baselinkeridsId);
}

